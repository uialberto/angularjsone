'use strict';
var util = require('util'),
  bunyan = require('bunyan'),
  path = require('path'),
  dir = path.resolve(__dirname, '../logs');


module.exports = function(app) {
  var isDebug = app.cfg.isDebug;
  return {
    main: createLogger('main', isDebug ? 'trace' : 'info','server.log'),
    access: createLogger('access', 'info', 'access.log'),
    notFound: createLogger('notFound', 'info', 'notFound.log'),
    sql: createLogger('sql', isDebug ? 'trace' : 'info', 'sql.log')
  };
};

function createLogger(name, level, fileName) {
  return bunyan.createLogger({
    name: name,
    streams: [{
      level: level || 'info',
      path: path.join(dir, fileName)
    }],
    serializers: {
      req: reqSerializer,
      err: errSerializer,
      sql: sqlSerializer
    }
  });
}
function reqSerializer(req) {
  return {
    method: req.method,
    url: req.url,
    headers: req.headers
  };
}
function errSerializer(err) {
  return {
    message: err.message,
    stack: err.stack,
    status: err.status
  };
}
function sqlSerializer(sql) {
  return sql;
}
