'use strict';

module.exports = function(app) {
  return {
    iterations: 1004,
  	keyLength: 36,
  	maxAttempts: 3,
  	tknLength: 12,
  	saltLength: 32,
  	tknWindow: {
      value: 60,
      unit: 'minutes'
    },
  	genLength: 10,
  	minLength: 7,
  	minHistory: 5,
  	minGroups: 3,
    newPassword: 'a1234567*'
  };
};
