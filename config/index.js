'use strict';

module.exports = function(app) {
  var cfg = app.cfg = {
    env: process.env.NODE_ENV || 'development',
    port: process.env.NODE_PORT || 3000,
    pushState: '/ui/*'
  };
  config('db');
  config('logs');
  config('password');
  config('session');

  function config(name) {
    console.log('config ' + name);
    cfg[name] = require('./' + name + '.js')(app);
  }
};
