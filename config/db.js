'use strict';

module.exports = function(app) {
  /* jshint maxcomplexity: 50 */
  return {
    host: process.env.MYSQL_PORT_3306_TCP_ADDR  || process.env.DB_PORT_3306_TCP_ADDR  || 'localhost',
    port: process.env.MYSQL_PORT_3306_TCP_PORT  || process.env.DB_PORT_3306_TCP_PORT  || 3306,
    user: process.env.NODE_DB_USER              || 'admin',
    password: process.env.NODE_DB_PASS          || 'Password',
    db: process.env.NODE_DB_USES                || 'angularcourse', // Maria
    database: process.env.NODE_DB_USES          || 'angularcourse', // MySql
    dialect: process.env.NODE_DB_DIALECT        || 'MySql',
    charset: 'utf8_spanish_ci',
    multipleStatements: true,
    supportBigNumbers: true
  };
};
