'use strict';

module.exports = function(app) {
  return {
  	secret: 'AngularJS Course',
  	rolling: false,
  	resave: false,
  	saveUninitialized: false,
    cookie: {
      path: '/',
      httpOnly: true,
      secure: false,
      maxAge: null
    }
  };
};
