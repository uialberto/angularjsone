'use strict';
var util = require('util'),
  http = require('http');

module.exports = function(app) {

  boot('utils');
  boot('promise');
  boot('express');
  boot('model');
  boot('app');
  boot('errors');
  boot('start');

  function boot(name) {
    console.log('boot ' + name);
    try {
      require('./' + name + '.js')(app);
    } catch(e) {
      console.error(e.stack || e);
    }
  }
};
