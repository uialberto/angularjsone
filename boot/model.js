'use strict';
var util = require('util');
var path = require('path');
var express = require('express');
var moment = require('moment');

module.exports = function(app) {
  var mw = app.middleware;
  var utils = app.libs.utils;
  class Model {
    constructor(name) {
      this.name = name;
      this.filename = path.resolve(__dirname, '../db', name + '.json');
      this.api = express.Router();
      app.api.use('/' + name, this.api);
      this.api.del = this.api['delete'];
      app.models[name] = this;
      this.route();
    }
    loadFromFile() {
      return app.promise.readFile(this.filename, 'utf8')
      .then(function(data) {
        var list = JSON.parse(data);
        return Promise.resolve(list);
      });
    }
    saveToFile(list) {
      var json = JSON.stringify(list, null, '  ');
      return app.promise.writeFile(this.filename, json, 'utf8');
    }
    route() {
      this.api.get('/', mw.noCache, this.search.bind(this));
      this.api.get('/:id', mw.noCache, this.findById.bind(this));
      this.api.post('/', mw.noCache, mw.bodyJson, this.insert.bind(this));
      this.api.put('/:id', mw.noCache, mw.bodyJson, this.update.bind(this));
      this.api.del('/:id', mw.noCache, this.del.bind(this));
    }
    search(req, res, next) {
      return this.loadFromFile()
      .done(function(list) {
        res.json(list);
      }, next);
    }
    indexOfId(list, id) {
      for(var k=0,n=list.length; k<n; k++) {
        var item = list[k];
        if (item.id === id) {return k;}
      }
      return -1;
    }
    compareIds(a, b) {
      return a.id - b.id;
    }
    findById(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      return this.loadFromFile()
      .done(function(list) {
        var index = self.indexOfId(list, req.params.id | 0);
        var item = index >= 0 ? list[index] : null;
        res.json(item);
      }, next);
    }
    insert(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = moment().format('YYYYMMDDHHmmss') | 0;
      return this.loadFromFile()
      .then(function(list) {
        var body = req.body;
        body.id = id;
        list.push(body);
        list.sort(self.compareIds);
        return self.saveToFile(list);
      }).done(function() {
        res.json({id:id});
      }, next);
    }
    update(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = req.params.id | 0;
      return this.loadFromFile()
      .then(function(list) {
        var index = self.indexOfId(list, id);
        if (index >= 0) {
          list[index] = req.body;
          return self.saveToFile(list);
        } else {
          return Promise.reject(utils.extend(new Error('NOT FOUND'), {status:404}));
        }
      }).done(function() {
        res.json({id:id});
      }, next);
    }
    del(req, res, next) {
      /* jshint bitwise: false */
      var self = this;
      var id = req.params.id | 0;
      return this.loadFromFile()
      .then(function(list) {
        var index = self.indexOfId(list, id);
        if (index >= 0) {
          list.splice(index, 1);
          return self.saveToFile(list);
        }
        return Promise.resolve(null);
      }).done(function() {
        res.json({id:id});
      }, next);
    }
  }
  app.Model = Model;
};
