'use strict';
var http = require('http');

module.exports = function(app) {
  http.createServer(app.web).listen(app.cfg.port, started(app.cfg.port));

  function started(port) {
    return function(e) {
      if (e) {
        console.error(e);
      } else {
        console.log('App running in port ' + port);
      }
    };
  }
};
