'use strict';
var path = require('path'),
	util = require('util'),
	crypto = require('crypto'),
	express = require('express'),
	favicon = require('serve-favicon'),
	logger = require('morgan'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	formidable = require('formidable'),
	layout = require('express-ejs-layouts');

module.exports = function(app) {
  var web = app.web = express();
  var api = app.api = express.Router();
  web.set('etag', false);
  web.set('views', path.resolve(__dirname, '../views'));
  web.set('view engine', 'ejs');
  web.use(favicon(path.join('www', 'img', 'favicon', 'favicon.ico')));
  web.use(logger('dev', {
    immediate: false
  }));
  web.use(cookieParser());
  web.use(express.static('www'));
  web.use(session(app.cfg.session));
	web.use(mwUser);
  app.middleware = {
    bodyJson: bodyParser.json(),
    noCache: mwNoCache,
    multipart: mwMultipart,
    setXSRF: mwSetXSRF,
    checkXSRF: mwCheckXSRF
  };
  web.use('/api', api);
	web.get('/ui/*', pushStateHandler);

  function mwMultipart(req, res, next) {
  	var form = new formidable.IncomingForm();
  	form.parse(req, function(err, fields, files) {
  		if (err) {return next(err);}
  		req.multipart = {
  			fields: fields,
  			files: files
  		};
  		next();
  	});
  }
  function mwNoCache(req, res, next) {
  	res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
  	res.header('Pragma', 'no-cache');
  	res.header('Expires', 0);
  	next();
  }
  function mwSetXSRF(req, res, next) {
  	if (req.session.XSRF) {
  		return next();
  	}
  	app.promise.randomBytes(20)
  		.then(function(buffer) {
  			var token = buffer.toString('hex');
  			app.log.main.trace({req,token}, 'XSRF');
  			req.session.XSRF = token;
  			res.cookie('XSRF-TOKEN', token, {
  				path: '/',
  				httpOnly: true
  			});
  			next();
  		}, next);
  }
  function mwCheckXSRF(req, res, next) {
  	var sToken = req.session.XSRF;
  	var hToken = req.cookies['XSRF-TOKEN'];
  	app.log.main.trace({req:req,session:sToken,cookie:hToken}, 'XSRF');
  	if (sToken !== hToken) {
  		app.log.main.error({sToken,hToken}, 'ERROR_XSRF');
      var err = new Error('ERROR_XSRF');
      err.status = 403;
  		next(err);
  	} else {
  		next();
  	}
  }
	function mwUser(req, res, next) {
		var user_id = req.session.user;
		req.user = null;
		if (!user_id) {
			return next();
		}
		app.models.user.loadFromFile()
		.then(function(list) {
			var found = list.filter(function(user) {
				return user.id === user_id;
			});
			if (found.length) {
				req.user = found[0];
			}
			next();
		}, next);
	}
	function sendFile(res, dirStatic) {
    res.sendFile(path.join(dirStatic, 'index.html'));
  }
  function pushStateHandler(req, res, next) {
    var dirStatic = path.resolve(__dirname, '../www');
    return sendFile(res, dirStatic);
  }
};
