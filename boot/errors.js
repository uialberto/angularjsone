'use strict';
var util = require('util');

module.exports = function(app) {
  var utils = app.libs.utils;
  app.web.use(notFoundHandler);
  app.web.use(errorHandler);

  function notFoundHandler(req, res, next) {
    var error = utils.extend(new Error('ERROR_NOT_FOUND'), {status:404});
    console.error(util.format('Route not found %s %s', req.method, req.url));
    next(error);
  }
  function errorHandler(err, req, res, next) {
    if (typeof(err) === 'string') {
      err = {
        message: err
      };
    }
    err.status = err.status || 500;
    if (err.status !== 404) {
      console.error(err);
    }
    var error = {
      message: err.message,
      status: err.status
    };
    res.status(error.status);
    res.json(error);
  }
};
