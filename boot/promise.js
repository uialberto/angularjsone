'use strict';
var fs = require('fs'),
  crypto = require('crypto');

if (!Promise.prototype.done) {
  Promise.prototype.done = function(onFulfilled, onRejected) {
    var self = arguments.length ? this.then.apply(this, arguments) : this;
    self.then(null, function (err) {
      setTimeout(function () {
        throw err;
      }, 0);
    });
  };
}
if (!Promise.denodeify) {
  Promise.denodeify = function (fn, argumentCount) {
    argumentCount = argumentCount || Infinity;
    return function () {
      var self = this;
      var args = Array.prototype.slice.call(arguments, 0,
          argumentCount > 0 ? argumentCount : 0);
      return new Promise(function (resolve, reject) {
        args.push(function (err, res) {
          if (err) {
            reject(err);
          } else {
            resolve(res);
          }
        });
        var res = fn.apply(self, args);
        if (res &&
          (
            typeof res === 'object' ||
            typeof res === 'function'
          ) &&
          typeof res.then === 'function'
        ) {
          resolve(res);
        }
      });
    };
  };
}
/**
 * Sequence of Promises
 * @param  {Array|..Promise} promises Promises
 * @return {Promise}          Promise
 */
function sequence(promises) {
	if (!Array.isArray(promises)) {
		promises = [].slice.call(arguments);
	}
	var ready = null;
	promises.forEach(function(p) {
    if (!ready) {
      ready = p;
    } else {
      ready = ready.then(function() {
  			return p;
  		});
    }
	});
	return ready || Promise.resolve(null);
}
/**
 * Sequence of Functions that returns Promises
 * @param  {Array|...function} promises Functions that return Promises
 * @return {Promise}          Promise
 */
function sequenceFcns(promises) {
	if (!Array.isArray(promises)) {
		promises = [].slice.call(arguments);
	}
	var ready = null;
	promises.forEach(function(p) {
    if (!ready) {
      ready = p();
    } else {
      ready = ready.then(function() {
  			return p();
  		});
    }
	});
	return ready || Promise.resolve(null);
}

function tryPromise(fcn) {
  try {
    fcn();
    return Promise.resolve(true);
  } catch(e) {
    return Promise.reject(e);
  }
}

var iterations = 1002;
var keyLength = 36;
var digest = 'sha1';

module.exports = function(app) {
  function encPassword(salt, password) {
    return app.promise.pbkdf2(password, salt, iterations, keyLength, digest)
    .then(function(buffer) {
      return buffer.toString('base64');
    });
  }
  app.promise = {
    sequence: sequence,
    sequenceFcns: sequenceFcns,
    readFile: Promise.denodeify(fs.readFile),
    writeFile: Promise.denodeify(fs.writeFile),
    readDir: Promise.denodeify(fs.readdir),
    randomBytes: Promise.denodeify(crypto.randomBytes),
    pbkdf2: Promise.denodeify(crypto.pbkdf2),
    try: tryPromise,
    encPassword: encPassword
  };
};
