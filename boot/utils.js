'use strict';
var util = require('util');

module.exports = function(app) {
  class Utils {
    /**
     * Extends on object (target) with the properties of other objects (sources)
     * @param  {Object|Array} target  Target Object
     * @param  {...Object}    sources Sources to extend target
     * @return {Object}               Target
     */
    extend(target, sources) {
    	if (!Array.isArray(sources)) {
    		sources = [].slice.call(arguments, 1).filter(function(src) {
          return typeof(src) === 'object' && !!src;
        });
    	}
    	if (!target) { target = {}; }
    	sources.forEach(function(source) {
    		Object.keys(source).forEach(function(key) {
    			target[key] = source[key];
    		});
    	});
    	return target;
    }
    /**
     * Copy on object but only the specified properties
     * @param  {object}    src   Source
     * @param  {...String} props Property names
     * @return {object}          Copied Object
     */
    copyProps(src, props) {
      if (!Array.isArray(props)) {props = [].slice.call(arguments, 1);}
      var res = {};
      props.forEach(function(p) {
        if (src.hasOwnProperty(p)) {
          res[p] = src[p];
        }
      });
      return res;
    }
    /**
     * Extracts only the date part of another date
     * @param  {Date} source Source
     * @return {Date}          Date containing only the date part of source
     */
    onlyDate(source) {
      return new Date(source.getFullYear(), source.getMonth(), source.getDate());
    }
    /**
     * String Repeat
     * @param  {string} text  Text to repeat
     * @param  {number} count Number of repetitions
     * @return {string}       Repeated text
     */
    strRepeat(text, count) {
      return (new Array(count+1)).join(text);
    }
    /**
     * Replace all ocurrences
     * @param  {string} string  String to Replace
     * @param  {string|RegEx} find    What to find
     * @param  {string} replace What to replace
     * @return {string}         String with replaced ocurrences
     */
    replaceAll(string, find, replace) {
    	return string.split(find).join(replace);
    }
    /**
     * Checks whether a string starts with search
     * @param  {string} str    String to check
     * @param  {string} search String to check for
     * @return {boolean}        str starts with search?
     */
    startsWith(str, search) {
    	return str.slice(0, search.length) === search;
    }
    /**
     * Checks whether a string ends with search
     * @param  {string} str    String to check
     * @param  {string} search String to check for
     * @return {boolean}        str ends with search?
     */
    endsWith(str, search) {
    	return str.slice(-search.length) === search;
    }
    /**
     * Checks whether a string is null, undefined or empty
     * @param  {string}  str String to check
     * @return {Boolean}     str is null, undefined or empty?
     */
    isNullOrEmpty(str) {
    	return str === null || str === undefined || str === '';
    }
    /**
     * Formats an object according to a format string
     * @param  {string} frm   Format String. Property names should be enclosed in curly braces.
     * @param  {Object} obj   Object to format
     * @return {string}       Formatted string
     */
    formatObj(frm, obj) {
      var self = this;
    	var res = frm;
      var pattern = /{([a-z0-9_-]+)}/gi;
      return frm.replace(pattern, function(m,p) {
        if (obj.hasOwnProperty(p)) {
          var val = obj[p];
          if (self.isNullOrEmpty(val)) {
            return m;
          } else {
            return val.toString();
          }
        }
        return m;
      });
    }
    /**
     * Converts an object to an array of its values
     * @param  {Object} obj    Object to convert
     * @param  {?boolean} sorted Sorted by keys?
     * @return {Array}        Array of values
     */
    toArray(obj, sorted) {
    	if (!obj) { return []; }
    	if (Array.isArray(obj)) { return obj; }
    	var keys = Object.keys(obj);
    	if (sorted) { keys.sort(); }
    	return keys.map(function (v, k, l) {
    		return obj[v];
    	});
    }
    /**
     * Converts an object to an array of properties
     * @param  {Object} obj     Object to convert
     * @param  {?string} [keyName=key] item's key name
     * @param  {?string} [valName=val] item's value name
     * @return {Array}         Array of properties
     */
    toArrayObject(obj, keyName, valName) {
    	if (typeof(obj) !== 'object') { return []; }
    	var keys = Object.keys(obj);
    	if (!keyName) { keyName = 'key'; }
    	if (!valName) { valName = 'val'; }
    	var res = keys.map(function(key, index) {
    		var item = {};
    		item[keyName] = key;
    		item[valName] = obj[key];
    		item.index = index;
    		return item;
    	});
    	keys.forEach(function(key) {
    		res[key] = obj[key];
    	});
    	return res;
    }
    /**
     * Executes a OS command
     * @param  {string} cmd  Command
     * @param  {?Array} args Command arguments
     * @return {Object}      Child Process
     */
    execCmd(cmd, args) {
      var spawn = require('child_process').spawn,
    	child = spawn(cmd, args || []);
      return child;
    }
    /**
     * Checks whether a value is defined. Not null and not undefined.
     * @param  {any}  value Value to check
     * @return {Boolean}       value is defined?
     */
    isDefined(value) {
      return value !== null && value !== undefined;
    }
    /**
     * Is Integer?
     * @param  {any}  value Value to test
     * @return {Boolean}    Whether value is integer or not
     */
    isInteger(value) {
      return typeof(value) === 'number' && (value % 1 === 0);
    }
    /**
     * Include values into array
     * @param  {Array}  arr    Array
     * @param  {...any} values Values
     * @return {Array}         The final array
     */
    include(arr, values) {
      if (!Array.isArray(values)) {values = [].slice.call(arguments, 1);}
      values.forEach(function(val) {
        if (arr.indexOf(val) === -1) {
          arr.push(val);
        }
      });
      return arr;
    }
    /**
     * Exclude values from array
     * @param  {Array}  arr    Array
     * @param  {...any} values Values
     * @return {Array}         The final array
     */
    exclude(arr, values) {
      if (!Array.isArray(values)) {values = [].slice.call(arguments, 1);}
      values.forEach(function(val) {
        var index = arr.indexOf(val);
        if (index > -1) {
          arr.splice(index, 1);
        }
      });
      return arr;
    }
    /**
     * Set If not defined
     * @param  {any} cond  Condition Values
     * @param  {any} value Not Defined value
     * @return {any}       Defined Value
     */
    ifNotDefined(cond, value) {
      return this.isDefined(cond) ? cond : value;
    }
    /**
     * Create Objects
     * @param  {...Class} ctors Classes
     * @return {Objects[]}      Created Objects
     */
    create(ctors) {
      if (!Array.isArray(ctors)) {ctors = [].slice.call(arguments);}
      return ctors.map(function(Ctor) {
        //console.log('creating ' + Ctor.name);
        try {
          return new Ctor();
        } catch(e) {
          console.error(e.stack);
          throw e;
        }
      });
    }
    /**
     * Inspect Object
     * @param  {object} obj Object
     * @return {String}     Object inspection
     */
    inspect(obj, title) {
      var msg = [];
      msg.push(util.format('%s: %s', title || 'inspect', typeof(obj)));
      if (typeof(obj) === 'object') {
        var keys = [];
        for(let key in obj) {
          if (key) {
            keys.push(key);
          }
        }
        keys.sort();
        keys.forEach(function(key) {
          msg.push(util.format('  %s: %s', key, typeof(obj[key])));
        });
        Object.keys(obj).forEach(function(key) {

        });
      }
      return msg.join('\n');
    }
    define(value, defValue) {
      return this.isDefined(value) ? value : defValue;
    }
  }
  app.libs.utils = new Utils();
};
