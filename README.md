# Contenido #

1. Introducción
> * ¿Que es Angular JS?
> * Conocimientos Previos
> * Herramientas necesarias
> * Mi primer proyecto.

2. Plantillas (Templates)
> * ngBind
> * ngRepeat
> * ngIf / ngShow / ngHide
> * ngModel
> * ngSrc / ngHref

3. Modulos y Dependencias
> * Declaración
> * Utilización
> * Angular-UI (Bootstrap)
> * UI-Router

4. Vistas y Controladores
> * Enrutamiento
> * Plantilla
> * Controlador

5. Directivas
> * Declaración
> * Transclude
> * Replace
> * Link
> * Controller
> * Scope

6. Servicios
> * $http
> * $timeout
> * $interval
> * $location
> * $filter

7. Componentes
> * Declaración
> * Utilización
> * Ejemplos

8. Optimizaciones
> * Dependencias
> * Gulp
> * Minificación
> * Concatenación
> * Inyección
> * Lazy Load (templateCache)

## Next
* Pruebas (Jasmine, Protractor)
* Directivas con configuración
* Directivas ReactJS
* UI Router Views
* Web Workers