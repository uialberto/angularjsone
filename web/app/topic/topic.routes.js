/**
* Routes
**/
(function() {
  'use strict';
  angular.module('app.topic')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .when('app.topic', '/topic', {
      templateUrl: templateUrl('app/topic/list.tpl.html'),
      controller: 'TopicListCtrl as vm',
      title: 'Categorías'
    });
  }
})();
