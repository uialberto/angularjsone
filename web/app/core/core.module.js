/**
* Core Module
**/
(function() {
  'use strict';
  angular.module('app', [
    'app.core',
    'app.layout',
    'app.topic',
    'app.post'
  ]);
  angular.module('app.core', [
    'ngAnimate',
    'ngMessages',
    'ngSanitize',
    'ui.router',
  ]);
})();
