/**
* Core Run
**/
(function() {
  'use strict';
  angular.module('app.core')
  .run(runIndex);

  runIndex.$inject = ['$rootScope'];
  function runIndex($rootScope) {
    $rootScope.urlArgs = urlArgs;
    $rootScope.basePath = basePath;
  }
})();
