/**
* Routes Config
**/
(function() {
  'use strict';
  angular.module('app.core')
  .config(configRouter);

  configRouter.$inject = ['$locationProvider'];
  function configRouter($locationProvider) {
    $locationProvider.html5Mode(true);
  }
})();
