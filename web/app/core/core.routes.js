/**
* Core Routes
**/
(function() {
  'use strict';
  angular.module('app.core')
  .provider('state', StateProvider);

  StateProvider.$inject = ['$stateProvider', '$urlRouterProvider'];
  function StateProvider($stateProvider, $urlRouterProvider) {
    var self = this;
    this.$get = getState();
    this.when = stateWhen;
    this.else = stateElse;
    this.abs = stateAbs;
    this.resolveUser = resolveUser;

    function stateWhen(name, url, config) {
      config.url = url;
      $stateProvider.state(name, config);
      return self;
    }
    function stateAbs(name, url) {
      $stateProvider.state(name, {
        abstract: true,
        url: url,
        template: '<div ui-view layout="row" flex></div>'
      });
      return self;
    }
    function stateElse(url) {
      /* jshint devel: true */
      otherwise.$inject = ['$injector', '$location'];
      function otherwise($injector, $location) {
        var path = $location.path();
        var $state = $injector.get('$state');
        if (path.startsWith(url)) {
          return url;
        } else if (path === '/') {
          return url;
        }
        console.warn('route not supported', {path:path});
        location.href = path;
      }
      $urlRouterProvider.otherwise(otherwise);
      return self;
    }
    function getState() {
      return {};
    }
    resolveUser.$injecct = ['$rootScope', '$q'];
    function resolveUser($rootScope, $q) {
      var defer = $q.defer();
      if ($rootScope.user) {
        defer.resolve($rootScope.user);
      } else {
        defer.reject('No User');
      }
      return defer.promise;
    }
  }
})();
