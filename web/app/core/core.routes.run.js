/**
* Routes Run
**/
(function() {
  'use strict';
  angular.module('app.core')
  .run(runRoutes);

  runRoutes.$inject = ['$rootScope', '$location', '$state', 'env'];
  function runRoutes($rootScope, $location, $state, env) {
    var handlingStateChangeError = false;
    var stateCounts = {
      errors: 0,
      changes: 0
    };
    $rootScope.isLoadingRoute = false;
    $rootScope.loadedRoute = function() {
      $rootScope.isLoadingRoute = false;
    };
    $rootScope.reloadState = function(stateParams) {
      $state.go($state.current, stateParams || {}, {reload:true});
    };
    $rootScope.gotoState = function(state, stateParams) {
      $state.go(state, stateParams);
    };
    $rootScope.gotoHome = function() {
      $rootScope.gotoState('app.home');
    };

    $rootScope.$on('$stateChangeError', stateChangeError);
    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);
    $rootScope.$on('$viewContentLoading', viewContentLoading);
    $rootScope.$on('$viewContentLoaded', viewContentLoaded);

    function stateChangeError(event, toState, toParams, fromState, fromParams, error) {
      /* jshint devel: true */
      if (handlingStateChangeError) { return; }
      stateCounts.errors++;
      handlingStateChangeError = true;
      $rootScope.isLoadingRoute = false;
      var dest = (toState && (toState.title || toState.name || toState.loadedTemplateUrl)) ||
        'unknown target';
      var msg = 'Error routing to ' + dest + '.\n' +
        (error.statusText || '') + ': ' +
        (error.status || '');
      console.error(msg, toState);
      $location.path('/');
    }
    function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
      stateCounts.changes++;
      handlingStateChangeError = false;
      //$rootScope.isLoadingRoute = true;
      var pageTitle = utils.formatObj(env.appTitle, {
        pageTitle: toState.title || ''
      });
      $rootScope.pageTitle = document.title = pageTitle;
    }
    function viewContentLoading(e, stateName) {
      //console.log('viewContentLoading', [].slice.call(arguments));
      $rootScope.isLoadingRoute = true;
    }
    function viewContentLoaded(e, stateName) {
      //console.log('viewContentLoaded', [].slice.call(arguments));
      $rootScope.isLoadingRoute = false;
    }
  }
})();
