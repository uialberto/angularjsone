/**
* Post View Controller
**/
(function() {
  'use strict';
  angular.module('app.post')
  .controller('PostViewCtrl', PostViewCtrl);

  PostViewCtrl.$inject = ['$scope', '$stateParams', 'TopicService', 'PostService'];
  function PostViewCtrl($scope, $stateParams, TopicService, PostService) {
    var vm = this;
    vm.topic = null;
    vm.posts = [];
    vm.searchText = '';
    vm.modelAddMessage = null;
    vm.showAddMessage = showAddMessage;
    vm.addMessage = addMessage;
    vm.cancelMessage = cancelMessage;
    vm.like = like;
    init();

    function init() {
      TopicService.findById($stateParams.id)
      .then(function(topic) {
        vm.topic = topic;
        return updatePosts();
      }).then(function(posts) {
      }, $scope.fcnError('Posts'));
    }
    function updatePosts() {
      return PostService.list($stateParams.id)
      .then(function(posts) {
        vm.posts = posts;
        return posts;
      });
    }
    function showAddMessage() {
      vm.modelAddMessage = {
        title: null,
        content: null,
        likes: 0
      };
    }
    function addMessage() {
      vm.modelAddMessage.createdAt = new Date();
      vm.modelAddMessage.topic = vm.topic.id;
      PostService.insert(vm.modelAddMessage)
      .then(function(data) {
        return updatePosts();
      })
      .then(function(posts) {
        vm.modelAddMessage = null;
      }, $scope.fcnError('Posts'));
    }
    function cancelMessage() {
      vm.modelAddMessage = null;
    }
    function like(post) {
      post.likes += 1;
      PostService.update(post)
      .then(function(data) {
        return updatePosts();
      })
      .then(function() {

      }, $scope.fcnError('Post Like'));
    }
  }
})();
