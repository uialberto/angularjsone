/**
* Ppost Service
**/
(function() {
  'use strict';
  angular.module('app.post')
  .service('PostService', PostService);

  PostService.$inject = ['$http', 'env'];
  function PostService($http, env) {
    ModelService.call(this, $http, env, 'post');
  }
  utils.inherits(PostService, ModelService);
  var pPost = PostService.prototype;
  pPost.list = function(topic) {
    return this.$http.get(this.url(topic, 'posts')).then(this.dataData);
  };
})();
