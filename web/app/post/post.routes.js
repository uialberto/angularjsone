/**
* Post Routes
**/
(function() {
  'use strict';
  angular.module('app.post')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .abs('app.post', '/post')
    .when('app.post.view', '/:id/view', {
      templateUrl: templateUrl('app/post/view.tpl.html'),
      controller: 'PostViewCtrl as vm',
      title: 'Mensajes'
    })
    .when('app.post.new', '/:id/new', {
      templateUrl: templateUrl('app/post/new.tpl.html'),
      controller: 'PostNewCtrl as vm',
      title: 'Agregar Mensaje'
    });
  }

})();
