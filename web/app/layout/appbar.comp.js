/**
* App Bar Component
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .component('myAppBar', {
    templateUrl: templateUrl('app/layout/appbar.tpl.html'),
    controller: MyAppBarCtrl,
    controllerAs: 'ctrl',
    scope: false
  });

  MyAppBarCtrl.$inject = ['$scope', '$state', '$location', 'env', 'AuthService'];
  function MyAppBarCtrl($scope, $state, $location, env, AuthService) {
    var ctrl = this;
    ctrl.menus = [];
    ctrl.appImage = env.webUrl + basePath + 'img/favicon/android-chrome-192x192.png';
    ctrl.logoutUrl = AuthService.logoutUrl();
    ctrl.user = $scope.user;
    ctrl.pageTitle = null;
    $scope.$on('auth-changed', authChanged);
    $scope.$on('$stateChangeSuccess', stateChangeSuccess);

    function authChanged(e, user) {
      ctrl.user = user;
      updateMenus();
    }
    function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
      updateMenus();
    }
    function updateMenus() {
      var menus = [];
      addMenu(menus, 'Categorías', 'app.topic');
      addMenu(menus, 'Fútbol', 'app.post.view', {id:1});
      ctrl.menus = menus;
    }
    function addMenu(menus, text, state, params) {
      if (!ctrl.user) {return;}
      var url = $state.href(state, params).substr(2);
      var active = url === $location.path();
      var sParams = params ? ['(', JSON.stringify(params),  ')'].join('') : '';
      menus.push({
        text: text,
        url: url,
        state: state + sParams,
        active: active
      });
    }
  }
})();
