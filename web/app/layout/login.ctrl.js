/**
* Login Controller
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .controller('LoginCtrl', LoginCtrl);

  LoginCtrl.$inject = ['$scope', '$state', 'AuthService'];
  function LoginCtrl($scope, $state, AuthService) {
    var vm = this;
    vm.username = null;
    vm.password = null;
    vm.onSubmit = onSubmit;

    function onSubmit(e) {
      if ($scope.frmLogin.$invalid) {return;}
      AuthService.login(vm.username, vm.password)
      .then(function(data) {
        $scope.$root.setAuth(data);
        $state.go('app.home');
      }, $scope.$root.fcnError('Ingresando'));
    }
  }
})();
