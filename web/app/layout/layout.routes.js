/**
* Routes
**/
(function() {
  'use strict';
  angular.module('app.core')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .else('/ui/')
    .abs('app', '/ui')
    .when('app.home', '/', {
      templateUrl: templateUrl('app/layout/home.tpl.html'),
      controller: 'HomeCtrl as vm',
      title: 'Bienvenido'
    })
    .when('app.login', '/login', {
      templateUrl: templateUrl('app/layout/login.tpl.html'),
      controller: 'LoginCtrl as vm',
      title: 'Ingresar'
    });
  }
})();
