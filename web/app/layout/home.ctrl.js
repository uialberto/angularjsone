/**
* Home Controller
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.$inject = ['$scope', 'TopicService'];
  function HomeCtrl($scope, TopicService) {
    var vm = this;
    vm.topics = [];

    TopicService.list()
    .then(function(list) {
      vm.topics = list;
    }, $scope.fcnError('Topics'));
  }
})();
