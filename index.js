'use strict';
// Ensure we're in the project directory, so relative paths work as expected
// no matter where we actually lift from.
process.chdir(__dirname);

var app = {
  libs: {},
  models: {}
};

require('./config')(app);
require('./boot')(app);
