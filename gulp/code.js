'use strict';

module.exports = function(gulp, cfg, libs) {
  var webSrc = cfg.src;
  var webDst = cfg.dst;
  var opt = cfg.optimize;
  var gif = libs.gif;
  gulp.task('www-code', [
    'www-code-app',
    'www-code-copy',
    'www-code-min',
    'www-code-polyfill'
  ]);
  gulp.task('www-code-app', ['www-resources', 'www-env'], function() {
    return gulp.src([
        webSrc + 'app/**/*.module.js',
        webSrc + 'app/**/*.js',
        '!' + webSrc + 'app/**/*.spec.js',
        '!' + webSrc + 'app/**/*.test.js'
      ])
    .pipe(gif(opt, libs.uglify()))
    .pipe(gif(opt, libs.concat('app.js')))
    .pipe(gulp.dest(webDst + 'app/'));
  });
  gulp.task('www-code-copy', ['www-resources'], function() {
    return gulp.src(webSrc + 'js/*.min.js')
    .pipe(gulp.dest(webDst + 'js/'));
  });
  gulp.task('www-code-min', ['www-resources'], function() {
    return gulp.src([
      webSrc + 'js/*.js',
      '!' + webSrc + 'js/*.polyfill.js',
      '!' + webSrc + 'js/*.min.js'
    ])
    .pipe(gif(opt, libs.uglify()))
    .pipe(gif(opt, libs.concat('build.js')))
    .pipe(gulp.dest(webDst + 'js/'));
  });
  gulp.task('www-code-polyfill', ['www-resources'], function() {
    return gulp.src([
      webSrc + 'js/*.polyfill.js'
    ])
    .pipe(gif(opt, libs.uglify()))
    .pipe(libs.concat('polyfill.js'))
    .pipe(gulp.dest(webDst + 'js'));
  });
};
