'use strict';

module.exports = function(gulp, cfg, libs) {
  var webSrc = cfg.src;
  var webDst = cfg.dst;
  var opt = cfg.optimize;
  var gif = libs.gif;
  gulp.task('www-env', ['www-resources'], function() {
    var src = [webSrc, 'app/core/env.', cfg.profile, '.json'].join('');
    return gulp.src(src)
    .pipe(libs.ngConfig('app', {createModule:false}))
    .pipe(libs.rename('env.config.js'))
    .pipe(gulp.dest(webSrc + 'app/core/'));
  });
};
