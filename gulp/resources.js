'use strict';

module.exports = function(gulp, cfg, libs) {
  var webSrc = cfg.src;
  var webDst = cfg.dst;
  var gif = libs.gif;
  var opt = cfg.optimize;
  gulp.task('www-resources', [
    'www-fonts',
    'www-bower',
    'www-css',
    cfg.optimize ? 'www-img-min' : 'www-img-all',
    'www-templates'
  ]);
  gulp.task('www-fonts', ['www-clean'], function() {
    return gulp
      .src(webSrc + 'fonts/*.*')
      //.pipe(libs.print())
      .pipe(gulp.dest(webDst + 'fonts/'));
  });
  gulp.task('www-bower', ['www-clean'], function() {
    return gulp
      .src(webSrc + '.bower_components/**/*.*')
      .pipe(gulp.dest(webDst + '.bower_components/'));
  });
  gulp.task('www-less', ['www-clean'], function() {
    return gulp.src(webSrc + 'styles/less/main.less')
    .pipe(gif(cfg.verbose, libs.print()))
    .pipe(gif(cfg.optimize, libs.plumber()))
    .pipe(libs.less())
    .pipe(libs.rename('less.css'))
    .pipe(gulp.dest(webSrc + 'styles/'));
  });
  gulp.task('www-css', ['www-less'], function() {
    return gulp.src([
      webSrc + 'styles/css/**/*.css',
      webSrc + 'styles/less.css'
    ])
    .pipe(gif(cfg.verbose, libs.print()))
    .pipe(gif(cfg.optimize, libs.plumber()))
    .pipe(gif(cfg.optimize, libs.autoprefixer({browser: ['last 2 version', '> 3%']})))
    .pipe(gif(cfg.optimize, libs.cssnano()))
    .pipe(libs.concat('main.css'))
    .pipe(gulp.dest(webDst + 'styles/'));
  });
  gulp.task('www-img-all', ['www-clean'], function() {
    return gulp.src(webSrc + 'img/**/*.*')
    .pipe(gulp.dest(webDst + 'img/'));
  });
  gulp.task('www-img-cpy', ['www-clean'], function() {
    return gulp.src(webSrc + 'img/**/*{.svg,.json,.xml,.ico}')
    .pipe(gulp.dest(webDst + 'img/'));
  });
  gulp.task('www-img-min', ['www-img-cpy'], function() {
    return gulp.src(webSrc + 'img/**/*{.png,.jpg,.jpeg,.gif}')
    .pipe(libs.imageop({
        optimizationLevel: 5,
        progressive: true,
        interlaced: true
      }))
    .pipe(gulp.dest(webDst + 'img/'));
  });
  gulp.task('www-templates', ['www-clean'], function() {
    return gulp.src(webSrc + 'app/**/*.html')
    .pipe(gif(opt, libs.htmlMin({
      collapseWhitespace:true
    })))
    .pipe(gif(opt, libs.templateCache(
      'templates.js',
      {
        module: 'app.layout',
        standalone: false,
        root: 'app/',
        templateHeader:
          '/**\n* Template Cache\n*/\n(function() {\n\t\'use strict\';\n\t/* jshint maxlen:false */\n' +
          '\tangular.module(\'<%= module %>\')\n\t.run([\'$templateCache\', function($templateCache) {',
        templateBody: '\n\t\t$templateCache.put(\'' + cfg.path + '<%= url %>?v=' +
          cfg.now + '\',\'<%= contents %>\');',
        templateFooter: '\n\t}]);\n})();'
      }
    )))
    .pipe(gulp.dest(webDst + (opt ? 'js/' : 'app/')));
  });
};
