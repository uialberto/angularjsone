'use strict';
// id, topic, title, content, likes

module.exports = function(app) {
  var mw = app.middleware;
  var utils = app.libs.utils;
  class Post extends app.Model {
    constructor() {
      super('post');
    }
    route() {
      this.api.get('/:topic/posts', mw.noCache, this.search.bind(this));
      this.api.get('/:id', mw.noCache, this.findById.bind(this));
      this.api.post('/', mw.noCache, mw.bodyJson, this.insert.bind(this));
      this.api.put('/:id', mw.noCache, mw.bodyJson, this.update.bind(this));
      this.api.del('/:id', mw.noCache, this.del.bind(this));
    }
    search(req, res, next) {
      /* jshint bitwise: false */
      var topic = req.params.topic | 0;
      return this.loadFromFile()
      .done(function(list) {
        var posts = list.filter(function(post) {
          return post.topic === topic;
        }).filter(function(post, idx) {
          return true; //idx < 20; // limit
        });
        res.json(posts);
      }, next);
    }
  }
  app.Post = Post;
  return Post;
};
