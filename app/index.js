'use strict';

module.exports = function(app) {
  var utils = app.libs.utils;
  init('auth');
  init('user');
  init('topic');
  init('post');

  function init(name) {
    console.log('app ' + name);
    try {
      var Ctor = require('./' + name + '.js')(app);
      var obj = new Ctor();
    } catch(e) {
      console.error(e.stack || e);
    }
  }
};
