'use strict';
var util = require('util');
var path = require('path');
var express = require('express');
var moment = require('moment');
var RSA = require('node-rsa');

module.exports = function(app) {
  var mw = app.middleware;
  var utils = app.libs.utils;
  class Auth {
    constructor() {
      this.api = express.Router();
      app.api.use('/auth', this.api);
      app.models.auth = this;
      this.rsa = new RSA({b:512});
      this.publicPEM = this.rsa.getPublicPEM();
      this.route();
    }
    route() {
      this.api.get('/me', mw.noCache, this.me.bind(this));
      this.api.get('/pem', mw.noCache, this.pem.bind(this));
      this.api.post('/login', mw.noCache, mw.bodyJson, this.login.bind(this));
      this.api.get('/logout', mw.noCache, this.logout.bind(this));
    }
    me(req, res, next) {
      res.json(req.user);
    }
    pem(req, res, next) {
      res.json({key:this.publicPEM});
    }
    login(req, res, next) {
      var errorMessage = 'Usuario y/o Contraseña no válidos';
      var username = req.body.username;
      var password = this.rsa.decrypt(req.body.password).toString();
      console.log(util.format('login: %s', username));
      if (!username || !password) {
        return next(new Error(errorMessage));
      }
      app.models.user.loadFromFile()
      .then(function(list) {
        var found = list.filter(function(user) {
          return user.username === username && user.password === password;
        });
        console.log('login found: ' + !!found.length);
        if (found.length) {
          var user = found[0];
          req.session.user = user.id;
          res.json(user);
        } else {
          req.session.user = null;
          next(utils.extend(new Error(errorMessage), {status:403}));
        }
      }, next);
    }
    logout(req, res, next) {
      req.session.destroy(function(err) {
        if (err) {
          return next(err);
        } else {
          res.redirect('/');
        }
      });
    }
  }
  return Auth;
};
