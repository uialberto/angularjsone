'use strict';
// id, title, description

module.exports = function(app) {
  class Topic extends app.Model {
    constructor() {
      super('topic');
    }
  }
  app.Topic = Topic;
  return Topic;
};
