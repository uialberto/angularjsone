'use strict';
// id, username, fullname, password, role

module.exports = function(app) {
  var utils = app.libs.utils;
  class User extends app.Model {
    constructor() {
      super('user');
    }
    findByName(name) {
      return this.loadFromFile()
      .then(function(list) {
        return list.hasOwnProperty(name) ? list[name] : null;
      });
    }
  }
  app.User = User;
  return User;
};
